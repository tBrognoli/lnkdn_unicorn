import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from settings import accounts, users
from Lnkdn_Unicorn.spiders.linkedin_spider import Linkedin_spider

process = CrawlerProcess(get_project_settings())

process.crawl(Linkedin_spider, accounts=accounts, users=users)
process.start()  # the script will block here until the crawling is finished
