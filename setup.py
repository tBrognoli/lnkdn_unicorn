"""Fichier d'installation de notre script salut.py."""

from cx_Freeze import setup, Executable

# On appelle la fonction setup
setup(
    name="Linkedin_parser",
    version="0.1",
    include_files = ["settings.py"],
    description="Let's scrap linkedin",
    executables=[Executable("main.py")],
)
