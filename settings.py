# linkedin
# =========

# Les identifiants de vos comptes Linkedin sur lesquel vous voulez vous connectez, les comptes doivent être valide
# Le script se connectera sur un de ses comptes aléatoirement
# NE PAS UTILISER SON VRAI COMPTE LINKEDIN POUR RISQUE DE BAN
accounts = [
    {
        "email": "bernardLeHaricot@outlook.fr",
        "password":  "MotDePasse$*",
    },
]

# Le nom et nom de famille des personnes à scraper
# En cas de problème, vérifier la syntaxe de votre nom-prénom dans l'url de votre page de profil linkedin
users = [
    {
        "user_profile_url": "https://www.linkedin.com/in/philippe-brognoli/",
        "user_name": "Philippe",
        "user_first_name": "Brognoli"
    },
    {
        "user_profile_url": "https://www.linkedin.com/in/tbrognoli/",
        "user_name": "Thomas",
        "user_first_name": "Brognoli"
    }
]
