import csv
import datetime
import html
import json
import re
import urllib
import uuid
from random import choice

import scrapy
from bs4 import BeautifulSoup
from scrapy.selector import Selector


class Linkedin_spider(scrapy.Spider):
    name = "linkedin"
    url = "https://www.linkedin.com/"
    all_posts = {"posts": []}

    headers = {
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "accept-encoding": "gzip, deflate, sdch, br",
        "accept-language": "en-US,en;q=0.8,ms;q=0.6",
        "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36",
    }

    def __init__(self, *args, **kwargs):
        super(Linkedin_spider, self).__init__(*args, **kwargs)
        self.account = choice(kwargs.get("accounts"))
        self.users = kwargs.get("users")

    def start_requests(self):
        urls = ["https://www.linkedin.com/"]
        for url in urls:
            yield scrapy.Request(
                url=url,
                headers=self.headers,
                callback=self.parse,
                meta={"cookiejar": uuid.uuid4()},
            )

    def parse(self, response):
        return scrapy.FormRequest.from_response(
            response,
            headers=self.headers,
            formxpath="//form[@class='login-form' ]",
            formdata={
                "session_key": self.account["email"],
                "session_password": self.account["password"],
            },
            callback=self.pass_captcha,
            meta={"cookiejar": response.meta["cookiejar"]},
        )

    def get_input_value(self, name, tree):
        value = tree.xpath(f"//input[@name='{name}']/@value").extract_first()
        return value if value else ""

    # FIXME resolve captcha could be better than bypass it
    def pass_captcha(self, response):
        if "captcha" in response.url:
            return scrapy.FormRequest.from_response(
                response,
                headers=self.headers,
                formxpath="//form[@name='CaptchaV2ChallengeForm']",
                callback=self.go_to_user_activity_page,
                meta={"cookiejar": response.meta["cookiejar"]},
            )
        else:
            for user in self.users:
                user_name = user["user_name"]
                user_first_name = user["user_first_name"]
                csv_name = f"csv/{str(datetime.date.today())}_{user_name} {user_first_name}.csv"
                url = f"{user['user_profile_url']}detail/recent-activity/shares/"
                print("# " + url)
                yield scrapy.Request(
                    url=url,
                    headers=self.headers,
                    callback=self.parse_user_activity,
                    meta={
                        "cookiejar": response.meta["cookiejar"], "csv_name": csv_name},
                )

    def go_to_user_activity_page(self, response):
        for user in self.users:
            user_name = user["user_name"]
            user_first_name = user["user_first_name"]
            csv_name = f"csv/{str(datetime.date.today())}_{user_name} {user_first_name}.csv"
            url = f"{user['user_profile_url']}detail/recent-activity/shares/"
            print("&"+url)
            yield scrapy.Request(
                url=url,
                headers=self.headers,
                callback=self.parse_user_activity,
                meta={"cookiejar": response.meta["cookiejar"], "csv_name": csv_name},
            )

    def parse_user_activity(self, response):
        text = BeautifulSoup(html.unescape(response.text), "lxml")

        content = re.findall(
            r"\(V2&amp;MEMBER_SHARES,urn:li:activity:(\d*)\).,.urn:li:fs_feedUpdate:",
            str(text),
        )
        if not content:
            print("NO JSON FIND")
            return

        len_max = len(content)
        for ref in content:
            yield scrapy.Request(
                url=f"https://www.linkedin.com/feed/update/urn:li:activity:{ref}/",
                headers=self.headers,
                callback=self.parse_post,
                meta={
                    "cookiejar": response.meta["cookiejar"],
                    "csv_name": response.meta["csv_name"],
                    "len_max": len_max,
                },
            )

    def parse_post(self, response):
        text = BeautifulSoup(html.unescape(response.text), "lxml")
        content = re.search(r"({.data.:{.urn.[\s\S]*?}]})", str(text))
        if not content:
            print("NO JSON FIND")
            return

        su = content.group(1)

        while True:
            try:
                post = json.loads(su)  # try to parse...
                break  # parsing worked -> exit loop
            except Exception as e:
                unexp = int(re.findall(r"\(char (\d+)\)", str(e))[0])
                unesc = su.rfind(r'"', 0, unexp)
                su = su[:unesc] + r"\"" + su[unesc + 1 :]
                closg = su.find(r'"', unesc + 2)
                su = su[:closg] + r"\"" + su[closg + 1 :]

        results = {}
        results["likes"] = []
        results["comments"] = []
        for infos in post["included"]:
            if infos.get("commentary") and not results.get("text"):
                results["url"] = response.url
                results.update({"text": infos["commentary"]["text"]["text"]})

            if infos.get("likes"):
                nb_likes = infos["likes"]["paging"]["count"]

                if nb_likes == 0:
                    continue
                results["likes"] = []
                results.update({"nb_likes": nb_likes})

                for like in infos["likes"]["*elements"]:
                    user_id = re.search(r"urn:li:member:(\d*)", like).group(1)
                    results["likes"].append({"user_id": user_id})

            if (
                infos.get("$type")
                and infos.get("$type") == "com.linkedin.voyager.feed.Comment"
            ):
                comments = {
                    "text": infos["comment"]["values"][0]["value"],
                    "user_id": re.search(
                        r"urn:li:member:(\d*)", infos["commenter"]["urn"]
                    ).group(1),
                }
                results["comments"].append(comments)

        for infos in post["included"]:
            if results.get("likes"):
                for like in results["likes"]:
                    user_id = like.get("user_id")
                    if infos.get("objectUrn") == f"urn:li:member:{user_id}":
                        user_infos = {
                            "Prénom": infos["firstName"],
                            "Nom": infos["lastName"],
                            "Travail": infos["occupation"],
                        }
                        like.update(user_infos)

            if results.get("comments"):
                for comment in results["comments"]:
                    user_id = comment.get("user_id")
                    if infos.get("objectUrn") == f"urn:li:member:{user_id}":
                        user_infos = {
                            "Prénom": infos["firstName"],
                            "Nom": infos["lastName"],
                            "Travail": infos["occupation"],
                        }
                        comment.update(user_infos)

        self.all_posts["posts"].append(results)

        len_max = response.meta['len_max']
        print(f"{len(self.all_posts['posts'])}/{len_max}")

        if len(self.all_posts["posts"]) == len_max:
            csv_name = response.meta["csv_name"]
            csv_columns = ["text", "url", "nb_likes", "likes", "comments"]

            try:
                with open(csv_name, "w") as csvfile:
                    writer = csv.DictWriter(
                        csvfile, fieldnames=csv_columns, extrasaction="ignore"
                    )
                    writer.writeheader()
                    for data in self.all_posts["posts"]:
                        str_likes = ""
                        for like in data.get("likes"):
                            if like.get("user_id"):
                                del like["user_id"]
                            str_likes += " | ".join(
                                f"{key} : {value}" for key, value in like.items()
                            )
                            str_likes += "\n"
                        data["likes"] = str_likes

                        str_comments = ""
                        for comment in data.get("comments"):
                            if comment.get("user_id"):
                                del comment["user_id"]
                            str_comments += " | ".join(
                                f"{key} : {value}" for key, value in comment.items()
                            )
                            str_comments += "\n"
                        data["comments"] = str_comments
                        writer.writerow(data)
            except IOError:
                print("I/O error")
